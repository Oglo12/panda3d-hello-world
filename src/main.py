from numpy import pi, sin, cos

from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3

# Init the Game class.
class Game(ShowBase):
    def __init__(self):
        # Load all the default functions and models etc....
        ShowBase.__init__(self)

        # Disable the camera trackball controls.
        self.disableMouse()

        # Init a scene object with the environment.egg model loaded.
        self.scene = self.loader.loadModel("models/environment")
        self.scene.reparentTo(self.render) # Reparent it to the renderer to actually show up on the screen.

        # Set the scene scale and position. (Note that the Panda3D coordinate system works where X and Y go forward/backward and side to side, whilst Z is up/down.)
        self.scene.setScale(0.25, 0.25, 0.25)
        self.scene.setPos(-8, 42, 0)

        # Add the camera spin function to the background tasks manager.
        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")

        # Load and transform the panda actor.
        self.pandaActor = Actor("models/panda-model", {"walk": "models/panda-walk4"})
        self.pandaActor.setScale(0.005, 0.005, 0.005) # Set the scale.
        self.pandaActor.reparentTo(self.render) # Make it render.
        self.pandaActor.loop("walk") # Loop the walking animation.

        # Create the 4 lerp intervals needed for the panda to walk back and forth.
        posInterval1 = self.pandaActor.posInterval(13, Point3(0, -10, 0), startPos = Point3(0, 10, 0))
        posInterval2 = self.pandaActor.posInterval(13, Point3(0, 10, 0), startPos = Point3(0, -10, 0))

        hprInterval1 = self.pandaActor.hprInterval(3, Point3(180, 0, 0), startHpr = Point3(0, 0, 0))
        hprInterval2 = self.pandaActor.hprInterval(3, Point3(0, 10, 0), startHpr = Point3(180, 0, 0))

        # Create and play the sequence that coordinates the intervals.
        self.pandaPace = Sequence(posInterval1, hprInterval1, posInterval2, hprInterval2, name = "PandaPace")
        self.pandaPace.loop()

    # Define a function to spin the camera.
    def spinCameraTask(self, task):
        # Calculate degrees and radians.
        angleDegrees = task.time * 6.0
        angleRadians = angleDegrees * (pi / 180.0)

        # Set the camera position and rotation/orientation.
        self.camera.setPos(20 * sin(angleRadians), -20 * cos(angleRadians), 3)
        self.camera.setHpr(angleDegrees, 0, 0) # setHpr() is actually setting the rotation/orientation.

        # Return Task.cont to tell the background task manager to loop this task.
        return Task.cont

# Init the app variable with the game object.
app = Game()

# Run the game, and start the loop!
app.run()
